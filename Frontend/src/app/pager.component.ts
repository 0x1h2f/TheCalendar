import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pager',
  styles: [`
    .pager {
      margin-bottom: 10px;
    }
    .title {
      display: inline-block;
      text-align: center;
      width: 120px;
      color: #656565;
    }
  `],
  template: `
    <div class="pager">
      <button kendoButton [icon]="'arrow-chevron-left'" (click)="prev()"></button>
      <div class="title"><strong><ng-content></ng-content></strong></div>
      <button kendoButton [icon]="'arrow-chevron-right'" (click)="next()"></button>
      <button kendoButton (click)="today()" [primary]="true">Сегодня</button>
    </div>
  `
})

export class PagerComponent {
  @Output() onPrev = new EventEmitter<string>();
  @Output() onNext = new EventEmitter<string>();
  @Output() onToday = new EventEmitter<string>();

  prev() {
    this.onPrev.emit();
  }

  next() {
    this.onNext.emit();      
  }

  today() {
    this.onToday.emit();      
  }
}
