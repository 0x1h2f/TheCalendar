import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IMember, IEvent } from './lib';

@Component({
  selector: 'editor',
  styles: [`
    span {      
      font-weight: bold;
    }
    span, #members-label {
      margin: 0 8px;
    }
    .title {
      font-size: 18px;
    }
    textarea {
      resize: vertical;
    }
    .close {
      padding: 0 16px 16px 0;
    }
    .right {      
      float: right;
      padding: 0 8px;
    }
  `],
  template: `
    <div>
      <div class="close">
        <button class="k-button k-primary k-bare right" (click)="close()">&times;</button>
      </div>
      <form class="k-form" #eventForm="ngForm">
        <div class="k-form-field">
          <input class="k-textbox" placeholder="Название" 
            name="Title" [(ngModel)]="event.Title" *ngIf="!editMode" required/>
          <input type="hidden" name="Title" [(ngModel)]="event.Title" *ngIf="editMode"/>
          <span class="title" *ngIf="editMode">{{event.Title}}</span>
        </div>
        <div class="k-form-field">
          <span>{{event.Date.toLocaleString('ru', { day: 'numeric', month: 'long'})}}</span>
        </div>
        <div class="k-form-field">
          <kendo-multiselect [data]="multiselectData"
            [filterable]="true"
            placeholder="Участники"
            #customMultiSelect
            textField="Name"
            valueField="Id"
            (valueChange)="valueChange($event)"
            (filterChange)="filterChange($event)"
            name="Members" [(ngModel)]="event.Members"
            *ngIf="!editMode"
            required
          >
          <ng-template kendoComboBoxNoDataTemplate>
            <h4>
              Не найдено.<br>
              Для добавления, наберите текст и нажмите ENTER
            </h4>
          </ng-template>
          </kendo-multiselect>
          <label id="members-label" [for]="membrs" *ngIf="editMode">Участники</label>
          <span id="membrs" *ngIf="editMode">{{getMembersString(event.Members)}}</span>
        </div>
        <div class="k-form-field">
          <textarea class="k-textarea" placeholder="Описание" 
            name="Description" [(ngModel)]="event.Description"></textarea>
        </div>
        <div class="k-form-field">
          <button class="k-button" (click)="close()" *ngIf="!editMode">Отменить</button>
          <button class="k-button k-primary" (click)="add()" *ngIf="!editMode" 
            [disabled]="eventForm.invalid">Добавить</button>
          <button class="k-button" (click)="delete()" *ngIf="editMode">Удалить</button>
          <button class="k-button k-primary" (click)="edit()" *ngIf="editMode"
            [disabled]="eventForm.invalid">Изменить</button>
        </div>
      </form>
    </div>
  `
})

export class EditorComponent {
  @Input() event: IEvent;
  @Input() members: Array<IMember> = [];

  @Output() onClose = new EventEmitter();
  @Output() onAdd = new EventEmitter<IEvent>();
  @Output() onDelete = new EventEmitter<number>();
  @Output() onEdit = new EventEmitter<IEvent>();

  private editMode: boolean = false;
    
  private multiselectData: Array<IMember>;

  @ViewChild("customMultiSelect")
  private customMultiSelect: any;

  private customMember: IMember = null;

  ngOnInit() {
    if (this.event.Id !== -1) {
      this.editMode = true;
    }
    this.multiselectData = this.members.slice();
  }
  
  ngOnChanges() {
    this.multiselectData = this.members.slice();
  }

  public valueChange(value: any): void {
    if (this.customMember !== null) {
      this.customMultiSelect.value.push(this.customMember);
      this.customMultiSelect.selectedDataItems.push(this.customMember);
      this.customMember = null;
    }
  }

  public filterChange(filter: any): void {
    let filteredArray: Array<IMember> = 
      this.members.filter((s: IMember) => s.Name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);

    if (filteredArray.length === 0) {
      this.customMember = { Id: -1, Name: filter };
    } else {
      this.customMember = null;
    }

    this.multiselectData = filteredArray;
  }

  private getMembersString(membersArray: Array<IMember>): string {
    return membersArray.map((e) => e.Name).join(', ');
  }

  close() {
    this.onClose.emit();
  }

  add() {
    this.onAdd.emit(this.event);
  }

  delete() {
    this.onDelete.emit(this.event.Id);
  }

  edit() {
    this.onEdit.emit(this.event);
  }
}