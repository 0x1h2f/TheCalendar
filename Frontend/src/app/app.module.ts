import { LOCALE_ID, NgModule }  from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ButtonsModule }        from '@progress/kendo-angular-buttons';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { PopupModule } from '@progress/kendo-angular-popup';
import { GridModule } from '@progress/kendo-angular-grid';

import { HttpService } from './http.service';

import { AppComponent }         from './app.component';
import { DayComponent }         from './day.component';
import { EditorComponent }         from './editor.component';
import { PagerComponent }         from './pager.component';
import { SearchComponent }         from './search.component';
import { ToolbarComponent }         from './toolbar.component';
import { QuickEditorComponent }         from './quick-editor.component';


/* Loading CLDR data
 * http://www.telerik.com/kendo-angular-ui/components/internationalization/

import { load } from '@progress/kendo-angular-intl';

load(
  require("cldr-data/supplemental/likelySubtags.json"),
  require("cldr-data/supplemental/currencyData.json"),
  require("cldr-data/supplemental/weekData.json"),

  require("cldr-data/main/bg/numbers.json"),
  require("cldr-data/main/bg/currencies.json"),
  require("cldr-data/main/bg/dateFields.json"),
  require("cldr-data/main/bg/ca-gregorian.json"),
  require("cldr-data/main/bg/timeZoneNames.json")
);

*/

@NgModule({
  imports: [ 
    BrowserModule, ButtonsModule, BrowserAnimationsModule, FormsModule, HttpModule, 
    DropDownsModule, PopupModule, GridModule 
  ],
  declarations: [ 
    AppComponent, DayComponent, 
    EditorComponent, PagerComponent,
    SearchComponent, ToolbarComponent,
    QuickEditorComponent
  ],
  bootstrap:    [ AppComponent ],
  providers: [ HttpService ]
})
export class AppModule { }

