import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { IEvent } from './lib';

@Component({
  selector: 'search',
  styles: [`
    .search {
      display: inline-block;
    }
    .title {
      font-weight: bold;
    }
  `],
  template: `
    <div class="search">
      <span class="k-icon k-i-search"></span>
      <kendo-autocomplete 
        [data]="data" 
        [filterable]="true"
        valueField="Title"
        placeholder="Поиск..."
        (focus)="focus()"
        (valueChange)="valueChange($event)"
        (filterChange)="filterChange($event)"
        #autocomplete
      >
        <ng-template kendoAutoCompleteItemTemplate let-dataItem>
          <div>
            <div class="title">{{dataItem.Title}}</div>
            <div class="date">{{dataItem.Date.toLocaleString('ru', { day: 'numeric', month: 'long' })}}</div>
          </div>
        </ng-template>
        <ng-template kendoDropDownListNoDataTemplate>
            <h4>Не найдено</h4>
        </ng-template>
      </kendo-autocomplete>
    </div>
  `
})

export class SearchComponent {
  @Input() events: Array<IEvent> = [];

  @Output() onSearch = new EventEmitter<IEvent>();
  @Output() onFocus = new EventEmitter();

  private data: Array<IEvent>;

  @ViewChild('autocomplete') 
  private autocomplete: any;

  focus() {
    this.onFocus.emit();
  }

  public valueChange(value: any): void {
    this.filterChange(value);
    let event: IEvent = this.data[0];
    this.autocomplete.reset();
    this.onSearch.emit(event);
  }

  public filterChange(filter: any): void {
    let filteredArray: Array<IEvent> = 
      this.events.filter((s: IEvent) => s.Title.toLowerCase().indexOf(filter.toLowerCase()) !== -1);

    this.data = filteredArray;
  }
}
