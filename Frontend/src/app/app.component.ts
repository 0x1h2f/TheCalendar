import { Component, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpService } from './http.service';
import { 
  CMember, CEvent, 
  IMember, IEvent, IDay, IWeek,
  getBeginFirstWeekOfMonth,
  getEndLastWeekOfMonth,
  getMonth, getWeek,
  Capitalize
} from './lib';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
})

export class AppComponent {
  private month: Array<IWeek>;
  private date: Date = new Date;
  private firstWeek: Array<Date>;

  private events: Array<IEvent> = [];
  private members: Array<IMember> = [];

  private anchor: ElementRef;
  private edit: boolean = false;
  private selectedDay: IDay;

  private edit2: boolean = false;
  private anchor2: ElementRef;

  private allEvents: Array<IEvent> = [];

  constructor(private httpService: HttpService) { }

  getMonthString(): string {
    return Capitalize(this.date.toLocaleString('ru', { month: 'long', year: 'numeric' }));
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    let startDate = getBeginFirstWeekOfMonth(this.date);
    let endDate = getEndLastWeekOfMonth(this.date);
    this.httpService.getEvents(startDate, endDate)
      .subscribe(data => { 
        this.events = data.map((e: Object) => new CEvent(e));

        this.update();
        this.select(this.date);
      });

    this.httpService.getMembers()
      .subscribe(data => { 
        this.members = data.map((e: Object) => new CMember(e));
      });
    this.firstWeek = getWeek(startDate);
  }

  update() {
    let startDate = getBeginFirstWeekOfMonth(this.date);
    this.firstWeek = getWeek(startDate);
    let getDay = this.getDay.bind(this);
    this.month = getMonth(this.date, getDay);
  }

  getDay(date: Date): IDay {
    return { 
      date: date,
      event: this.events.find(p => p.Date.toDateString() === date.toDateString()) ||
        { Id: -1, Date: date, Title: "", Description: "", Members: [] },
      weekday: this.firstWeek.some(p => p.toDateString() === date.toDateString()),
      selected: this.selectedDay ? this.selectedDay.date.toDateString() === date.toDateString() : false,
      current: date.getMonth() === this.date.getMonth()
    };
  }
  
  updateData(date: Date) {    
    this.date = date;
    this.loadData();
  }

  onDaySelect(day: IDay) {
    this.select(day.date);
  }

  select(date: Date) {
    this.edit = false;
    this.edit2 = false;
    
    if (this.date.getMonth() == date.getMonth()) {
      this.date = date;

      this.month = this.month.map(week => {
        for(let prop in week) {
          if (week[prop].date.toDateString() === date.toDateString()) {
            week[prop].selected = true;
            this.selectedDay = week[prop];
          } else {
            week[prop].selected = false;
          }
        }
        return week;
      });
    } else {
      this.updateData(date);
    }
  }

  onDayDblClick(e: any) {
    this.anchor = e.target;
    this.edit = true;
  }

  onAdd(event: IEvent) {
    this.edit = false;
    this.httpService.addEvent(event)
      .subscribe((data) => {
        this.events.push(new CEvent(data));
        data.Members.forEach((member: IMember) => {
          if(!this.members.some(p => p.Id === member.Id)) {
            this.members.push(member);
          }
        });
        this.update();
      });
  }

  onEdit(event: IEvent) {
    this.edit = false;
    this.httpService.editEvent(event)
      .subscribe((data) => {
        data.Members.forEach((member: IMember) => {
          if(!this.members.some(p => p.Id === member.Id)) {
            this.members.push(member);
          }
        });
        this.update();
      });
  }

  onDelete(id: number) {
    this.edit = false;
    this.httpService.deleteEvent(id)
      .subscribe((data) => {
        if (data === id) {
          this.events = this.events.filter(p => p.Id !== id);
          this.update();
        }
      });
  }

  onClose() {
    this.edit = false;
  }

  onPrev() {
    this.updateData(new Date(this.date.getFullYear(), this.date.getMonth() - 1, this.date.getDate()));
  }

  onNext() {
    this.updateData(new Date(this.date.getFullYear(), this.date.getMonth() + 1, this.date.getDate()));
  }

  onToday() {
    this.updateData(new Date());
  }

  onAdd2(e: any) {
    this.anchor2 = e.target;
    this.edit2 = true;
    this.edit = false;
  }

  onRefresh() {
    this.loadData();
  }

  onCloseQuickEditor() {
    this.edit2 = false;
  }

  onAddQuickEditor(text: string) {
    this.edit2 = false;
    let data = text.split(',');
    let members = data.slice(2).map(name => {
        let member = this.members.find(s => s.Name === name.trim());
        return {
          Id: member ? member.Id : -1,
          Name: name.trim()
        };
    });
    let event: IEvent = {
      Id: -1,
      Date: new Date(data[0].trim()),
      Title: data[1].trim(),
      Description: '',
      Members: members
    };
    this.httpService.addEvent(event)
      .subscribe((data) => {
        this.events.push(new CEvent(data));
        data.Members.forEach((member: IMember) => {
          if(!this.members.some(p => p.Id === member.Id)) {
            this.members.push(member);
          }
        });
        this.update();
      });
  }

  onSearch(event: IEvent) {
    this.updateData(event.Date);
  }

  onFocusSearch() {
    this.httpService.getAllEvents()
      .subscribe(data => { 
        this.allEvents = data.map((o: Object) => new CEvent(o));
      });
  }
}