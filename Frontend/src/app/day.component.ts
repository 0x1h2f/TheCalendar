import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IDay, Capitalize } from './lib';

@Component({
  selector: 'day',
  styles: [`
    .day {
      border: 1px solid #ebebeb;
      height: 110px;
      padding: 8px;
      color: #656565;

      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      -o-user-select: none;
      user-select: none;
    }
    .selected {
      background-color: #ff6358 !important;
      color: #fff !important;
    }
    .today {
      height: 106px;
      border-bottom: 4px solid #ff6358;
    }
    .hovered {
      background-color: #f6f6f6;
    }
    .nocurrent {
      opacity: 0.6;
    }
    .hasEvent {
      background-color: #ededed;
    }
  `],
  template: `
    <div class="day" 
      [ngClass]="{ today: (today && !selected), 
        selected: day.selected, 
        hovered: (hovered && !day.selected),
        nocurrent: (!day.current && !day.selected),
        hasEvent: (hasEvent && !hovered && !day.selected) }"
      (click)="click()"
      (dblclick)="dblclick($event)"
      (mouseover)="mouseover()"
      (mouseout)="mouseout()"
    >
      <div>{{dayString}}</div>
      <div>{{eventString}}</div>
      <div>{{membersString}}</div>    
    </div>
  `
})

export class DayComponent {
  @Input() day: IDay;

  @Output() onSelect = new EventEmitter<IDay>();
  @Output() onDblClick = new EventEmitter<any>();

  private dayString: string = "";
  private eventString: string = "";
  private membersString: string = "";
  private today: boolean = false;
  private hovered: boolean = false;
  private hasEvent: boolean = false;

  ngOnInit() {
    this.today = this.day.date.toDateString() === (new Date).toDateString();
    let options;
    if (this.day.weekday) {
      options = { day: 'numeric', weekday: 'long' }
    } else {
      options = { day: 'numeric' }
    }
    this.dayString = Capitalize(this.day.date.toLocaleString('ru', options));
    if (this.day.event.Id !== -1) {
      this.eventString = this.day.event.Title;
      this.membersString = this.day.event.Members.map(p => p.Name).join(', ');
      
      this.hasEvent = true;
    }
  }

  click() {
    this.onSelect.emit(this.day);
  }

  dblclick(e: any) {
    this.onDblClick.emit(e);
  }

  mouseover() {
    this.hovered = true;
  }

  mouseout() {
    this.hovered = false;
  }
}
