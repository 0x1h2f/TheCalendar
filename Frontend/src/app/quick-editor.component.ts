import { Component, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'quick-editor',
  styles: [`
    .close {
      padding: 0 16px 16px 0;
    }
    .right {      
      float: right;
      padding: 0 8px;
    }
  `],
  template: `
    <div>
      <div class="close">
        <button class="k-button k-primary k-bare right" (click)="close()">&times;</button>
      </div>
      <form class="k-form" #eventForm="ngForm">
        <div class="k-form-field">
          <input class="k-textbox" 
            placeholder="2017-05-23, Съездить на шашлыки, Я, Джеки Чан, Жан Клод Ван Дамм, ..." 
            name="text" [(ngModel)]="text" required 
            pattern="[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])(,.*[a-zA-Zа-яА-Я].*){2}"
          />
        </div>
        <div class="k-form-field">
          <button class="k-button k-primary"
            [disabled]="eventForm.invalid"
            (click)="add()">Добавить</button>
        </div>        
      </form>
    </div>
  `
})

export class QuickEditorComponent {
  @Output() onClose = new EventEmitter();
  @Output() onAdd = new EventEmitter<string>();

  private text: string;

  close() {
    this.onClose.emit();
  }

  add() {
    this.onAdd.emit(this.text);
  }
}
