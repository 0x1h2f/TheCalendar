import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IEvent } from './lib';

@Component({
  selector: 'toolbar',
  styles: [`
    .toolbar {
      margin-bottom: 10px;
      color: #656565;
      background-color: #ededed;
      padding: 10px;
      margin-left: -8px;
      margin-right: -8px;
      margin-top: -8px;
    }
    .right {
      float: right;
    }
  `],
  template: `
    <div class="toolbar">
      <button kendoButton (click)="add($event)" [primary]="true">Добавить</button>
      <button kendoButton (click)="refresh()">Обновить</button>
      <search class="right"
        [events]="events" 
        (onSearch)="search($event)"
        (onFocus)="focus()"
      ></search>
    </div>
  `
})

export class ToolbarComponent {
  @Input() events: Array<IEvent> = []; 

  @Output() onAdd = new EventEmitter<any>();
  @Output() onRefresh = new EventEmitter();
  @Output() onSearch = new EventEmitter<any>();
  @Output() onFocusSearch = new EventEmitter();
  
  add(e: any) {
    this.onAdd.emit(e);
  }

  refresh() {
    this.onRefresh.emit();
  }

  search(event: IEvent) {
    this.onSearch.emit(event);
  }

  focus() {
    this.onFocusSearch.emit();
  }
}
