export interface IMember {
  Id: number,
  Name: string
}

export class CMember implements IMember{
  Id: number;
  Name: string;
  constructor(obj: Object) {
    this.Id = obj['Id'];
    this.Name = obj['Name'];
  }
}

export interface IEvent {
  Id: number,
  Date: Date,
  Title: string,
  Description: string,
  Members: Array<IMember>
}

export class CEvent implements IEvent {
  Id: number;
  Date: Date;
  Title: string;
  Description: string;
  Members: Array<IMember>;
  
  constructor(obj: Object) {
    this.Id = obj['Id'];
    this.Date = new Date(obj['Date']);
    this.Title = obj['Title'];
    this.Description = obj['Description'];
    this.Members = Array.prototype.map.call(obj['Members'], function(el: Object){
      return new CMember(el);
    });
  }
}

export interface IDay {
  date: Date,
  event: IEvent,
  weekday: boolean,
  selected: boolean
  current: boolean
}

export interface IWeek {
  monday: IDay
  tuesday: IDay
  wednesday: IDay
  thursday: IDay
  friday: IDay
  saturday: IDay
  sunday: IDay
}

export function Capitalize(s: string): string {
  return s.charAt(0).toUpperCase() + s.slice(1);
}

export function getFirstDateOfWeek(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
}

export function getLastDateOfWeek(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
}

export function getBeginFirstWeekOfMonth(date: Date) {
  return getFirstDateOfWeek(new Date(date.getFullYear(), date.getMonth(), 1));
}

export function getEndLastWeekOfMonth(date: Date) {
  return getLastDateOfWeek(new Date(date.getFullYear(), date.getMonth() + 1, 0));
}

export function getWeek(firstDayOfWeek: Date): Array<Date> {
  let week: Array<Date> = [];
  let date = firstDayOfWeek;

  week.push(date);
  for(let i = 1; i < 7; i++) {
    week.push(new Date(date.getFullYear(), date.getMonth(), date.getDate() + i));
  }

  return week;
}

export function getMonth(date: Date, getDay: any): Array<IWeek> {
  let beginFirstWeekOfMonth = getBeginFirstWeekOfMonth(date);
  let endLastWeekOfMonth = getEndLastWeekOfMonth(date);

  let weeks: Array<IWeek> = [];
  let _date = beginFirstWeekOfMonth;
  while(_date <= endLastWeekOfMonth) {
    let week = getWeek(_date);

    weeks.push({
      monday: getDay(week[0]),
      tuesday: getDay(week[1]),
      wednesday: getDay(week[2]),
      thursday: getDay(week[3]),
      friday: getDay(week[4]),
      saturday: getDay(week[5]),
      sunday: getDay(week[6])
    });
    
    _date = new Date(_date.getFullYear(), _date.getMonth(), _date.getDate() + 7);
  }
  return weeks;
}