import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as moment from 'moment/moment';

import { IMember, IEvent } from './lib';
import { Global } from './global';

const headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

Date.prototype.toJSON = function(){ return moment(this).format(); }

@Injectable()
export class HttpService{
 
  constructor(private http: Http){ }
  
  getAllEvents(){
    return this.http.get(Global.API_URL + 'event')
      .map((resp: Response) => resp.json())
      .catch((error:any) => { return Observable.throw(error); });
  }

  getEvents(startDate: Date, endDate: Date){
    return this.http.get(Global.API_URL + 'event/' + startDate.toDateString() + '/' + endDate.toDateString())
      .map((resp: Response) => resp.json())
      .catch((error:any) => { return Observable.throw(error); });
  }

  getMembers(){
    return this.http.get(Global.API_URL + 'member')
      .map((resp: Response) => resp.json())
      .catch((error:any) => { return Observable.throw(error); });
  }

  addEvent(event: IEvent) {
    const body = JSON.stringify(event);  
        
    return this.http.post(Global.API_URL + 'event', body, { headers: headers })
      .map((resp: Response) => resp.json())
      .catch((error:any) => { return Observable.throw(error); });
  }

  editEvent(event: IEvent) {
    const body = JSON.stringify(event);  
        
    return this.http.put(Global.API_URL + 'event/' + event.Id.toString(), body, { headers: headers })
      .map((resp: Response) => resp.json())
      .catch((error:any) => { return Observable.throw(error); });
  }

  deleteEvent(id: number) {
    return this.http.delete(Global.API_URL + 'event/' + id.toString())
      .map((resp: Response) => resp.json())
      .catch((error: any) => { return Observable.throw(error); });  
  }
}