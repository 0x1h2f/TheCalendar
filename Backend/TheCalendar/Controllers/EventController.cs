﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TheCalendar.Models;

namespace TheCalendar.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EventController : BaseApiController
    {
        public HttpResponseMessage Get()
        {
            return ToJson(db.Events.Include(p => p.Members).AsEnumerable());
        }

        public HttpResponseMessage Get(DateTime sdate, DateTime edate)
        {
            return ToJson(db.Events
                .Where(p => p.Date >= sdate && p.Date <= edate)
                .Include(p => p.Members).AsEnumerable());
        }

        public HttpResponseMessage Post([FromBody]Event value)
        {
            db.Events.Add(value);
            foreach (Member member in value.Members)
            {
                if (member.Id != -1 && db.Members.Any(p => p.Id == member.Id))
                {
                    db.Members.Attach(member);
                }
            }
            db.SaveChanges();
            return ToJson(value);
        }

        public HttpResponseMessage Put(int id, [FromBody]Event value)
        {
            db.Entry(value).State = EntityState.Modified;
            db.SaveChanges();
            return ToJson(value);
        }
        public HttpResponseMessage Delete(int id)
        {
            db.Events.Remove(db.Events.FirstOrDefault(x => x.Id == id));
            db.SaveChanges();
            return ToJson(id);
        }
    }
}
