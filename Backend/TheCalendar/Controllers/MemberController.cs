﻿using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TheCalendar.Models;

namespace TheCalendar.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MemberController : BaseApiController
    {
        public HttpResponseMessage Get()
        {
            return ToJson(db.Members.AsEnumerable());
        }

        public HttpResponseMessage Post([FromBody]Member value)
        {
            db.Members.Add(value);
            db.SaveChanges();
            return ToJson(value);
        }

        public HttpResponseMessage Put(int id, [FromBody]Member value)
        {
            db.Entry(value).State = EntityState.Modified;
            db.SaveChanges();
            return ToJson(value);
        }
        public HttpResponseMessage Delete(int id)
        {
            db.Members.Remove(db.Members.FirstOrDefault(x => x.Id == id));
            db.SaveChanges();
            return ToJson(1);
        }
    }
}
