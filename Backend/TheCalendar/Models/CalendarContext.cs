﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace TheCalendar.Models
{
    public class CalendarContext: DbContext
    {
        static CalendarContext()
        {
            Database.SetInitializer<CalendarContext>(new CalendarContextInitializer());
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Member> Members { get; set; }
    }

    class CalendarContextInitializer : DropCreateDatabaseAlways<CalendarContext>
    {
        protected override void Seed(CalendarContext db)
        {
            const int date_year = 2017;
            const int date_start_month_number = 6;
            const int date_end_month_number = 8;
            const int date_start_day_number = 1;
            const int date_end_day_number = 30;
            const int event_members_max_count = 5;

            string[] members_array = new string[] {
                "Я",
                "Том Харди",
                "Эмилия Кларк",
                "Марго Робби",
                "Хлоя Грейс Морец",
                "Кара Делевинь",
                "Элизабет Олсен",
                "Крис Пратт",
                "Скарлетт Йоханссон",
                "Леонардо ДиКаприо",
                "Джаред Лето",
                "Бенедикт Камбербэтч",
                "Галь Гадот",
                "Дуэйн Джонсон"
            };

            string[] events_array = new string[] {
                "Сходить в спортзал",
                "Cлетать в Лас Вегас",
                "Поход в Гималаи",
                "Сходить на футбол",
                "Посетить музыкальный фестиваль",
                "Отдых на Гавайях",
                "Сходить на концерт",
                "Сходить на премьеру фильма Лига справедливости",
                "Проехать Европу автостопом",
                "Прыгнуть с паршюта",
                "Переплыть Тихий океан на яхте",
                "Нырнуть в Марианскую впадину",
                "Слетать на Марс",
            };

            Random r = new Random();

            for (int i = 0; i < members_array.Length; i++)
            {
                db.Members.Add(new Member
                {
                    Name = members_array[i] 
                });
            }
            db.SaveChanges();

            for (int i = 0; i < events_array.Length; i++)
            {
                List<Member> members = new List<Member>();
                members.Add(db.Members.FirstOrDefault(p => p.Name == "Я"));
                for (int j = 0; j < r.Next(1, event_members_max_count); j++)
                {
                    int id = r.Next(1, members_array.Length + 1);
                    if (!members.Any(p => p.Id == id))
                    {
                        members.Add(db.Members.FirstOrDefault(p => p.Id == id));
                    }
                }

                db.Events.Add(new Event
                {
                    Date = new DateTime(
                        date_year,
                        r.Next(date_start_month_number, date_end_month_number + 1),
                        r.Next(date_start_day_number, date_end_day_number + 1)
                        ),
                    Title = events_array[i],
                    Description = "«" + events_array[i].ToUpper() + "»",
                    Members = members
                });
            }
            db.SaveChanges();

        }

    }
}