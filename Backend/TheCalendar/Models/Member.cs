﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TheCalendar.Models
{
    public class Member
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Event> Events { get; set; }
    }
}