﻿using System;
using System.Collections.Generic;

namespace TheCalendar.Models
{
    public class Event
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<Member> Members { get; set; }
    }
}